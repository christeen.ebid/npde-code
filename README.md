# Code material for NPDE cycle (Christoph Lehrenfeld)

Here, we provide additional codes and numerical examples for the lectures, exercises and tutorials of the lecture cycle on the numerics of partial differential equations at the University of Göttingen.

# Content 
The coding material / examples are stored in the `content`-directory.

# Deployment and use of the code examples

## jupyter lite
One easy-to-use scenario is the use of jupyter-lite. 

We use the pyodide-instance generated from https://gitlab.gwdg.de/lehrenfeld/ngsx-pyodide to run [`NGSolve`](www.ngsolve.org) in your browser. The material of this repository is deployed at https://lehrenfeld.pages.gwdg.de/npde-code/ .

Note that the jupyterlite-instance is not persistent, it's only living in your browser cache. If you want to save your work, you can (and have to) download the notebook and upload it again later. If you want to have a persistent user experience you will have to use one of the following options:

## jupyter gwdg

You can use the gwdg instance of jupyter at https://jupyter-cloud.gwdg.de/ . You can log in with your gwdg-account. 
You can then upload the notebooks from this repository and work with them.
To install ngsolve, simply run
`!pip install ngsolve` and `!pip install webgui_jupyter_widgets` in a cell.

## local install

You can also install NGSolve locally on your machine. See https://ngsolve.org/downloads for further instructions.